import socket
import cv2
import numpy as np
import os


staging_server = socket.socket(
    socket.AF_INET,
    socket.SOCK_STREAM,
)

staging_server.bind(

    ('localhost', 9090)
)

staging_server.listen(1)
print("Server is listening")


while True:
    try:
        client_socket, client_address = staging_server.accept()

        data1 = client_socket.recv(1024)
        sizeOfImage1 = int(data1.decode('utf-16'))
        print("Size of input image:", sizeOfImage1)

        file = open('images/server_img.png', mode="wb")
        data1  = client_socket.recv(1024)

        while data1 :
            file.write(data1 )
            data1  = client_socket.recv(1024)

        file.close()

        image = cv2.imread('images/server_img.png')

        row, col, ch = image.shape
        mean = 0
        var = 100
        sigma = var ** 0.5
        gauss = np.random.normal(mean, sigma, (row, col, ch))
        print('Максимальная величина вносимого шума: ', np.max(gauss))
        noisy = image + gauss

        cv2.imwrite('images/img_output.png', noisy)

        file = open('img_output.png', mode="rb")

        imageSize = os.path.getsize('images/img_output.png')
        print("Size of output image:", imageSize)
        staging_server.send(f"{imageSize}".encode('utf-16'))

        data = file.read(1024)

        while imageSize > 0:
            data = file.read(1024)
            staging_server.send(data)
            if (imageSize >= 1024):
                imageSize = imageSize - 1024
            else:
                imageSize = imageSize - imageSize

        file.close()
        print("Картинка была отправлена", '\n')
    except:
        print('\n[Сервер остановлен]')
        break

staging_server.close()