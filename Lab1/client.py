import os
import socket

client = socket.socket(
    socket.AF_INET,
    socket.SOCK_STREAM,
)

client.connect(
    ('localhost', 9090)
)

file = open('images/img.png', mode="rb")

imageSize = os.path.getsize('images/img.png')
print("Size of image:", imageSize)
client.send(str(imageSize).encode('utf-16'))

data = file.read(1024)

while data:
    client.send(data)
    data = file.read(1024)

file.close()
client.close()